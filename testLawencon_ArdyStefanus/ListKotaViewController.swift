//
//  ListKotaViewController.swift
//  testLawencon_ArdyStefanus
//
//  Created by Ardy Stefanus Sunarno on 18/02/21.
//

import UIKit

class ListKotaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableViewList: UITableView!
    var tempListCity:[String] = ["Malang", "Mojokerto", "Surabaya", "Bali", "Jakarta"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableViewList.dataSource = self
        tableViewList.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        tempListCity.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableViewList.dequeueReusableCell(withIdentifier: "cellCityIdentifier", for: indexPath) as! CityCell
        cell.titleCity.text = tempListCity[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let detailvc = storyboard?.instantiateViewController(withIdentifier: "detailCityVC") as! ListDetailViewController
        detailvc.tempIndex = indexPath.row
        navigationController?.pushViewController(detailvc, animated: true)
    }
    
    @IBAction func logOut(_ sender: UIButton)
    {
        UserDefaults.standard.set(false, forKey: "ISUSERLOGGEDIN")
        self.navigationController?.popToRootViewController(animated: true)
    }
}

class CityCell: UITableViewCell
{
    @IBOutlet weak var titleCity: UILabel!
}
