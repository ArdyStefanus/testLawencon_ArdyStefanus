//
//  ListDetailViewController.swift
//  testLawencon_ArdyStefanus
//
//  Created by Ardy Stefanus Sunarno on 18/02/21.
//

import UIKit

class ListDetailViewController: UIViewController {

    @IBOutlet weak var textLabelKota: UILabel!
    
    var tempListCity:[String] = ["Malang", "Mojokerto", "Surabaya", "Bali", "Jakarta"]
    var tempIndex:Int = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textLabelKota.text = tempListCity[tempIndex]
    }
}
