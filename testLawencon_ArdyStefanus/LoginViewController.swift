//
//  ViewController.swift
//  testLawencon_ArdyStefanus
//
//  Created by Ardy Stefanus Sunarno on 18/02/21.
//

import UIKit

class LoginViewController: UIViewController
{

    @IBOutlet weak var txtUserField: UITextField!
    @IBOutlet weak var txtPassField: UITextField!
    @IBOutlet weak var labelAlert: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        labelAlert.isHidden = true
        
        if UserDefaults.standard.bool(forKey: "ISUSERLOGGEDIN") == true
        {
            let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapVC") as! MapViewController
            self.navigationController?.pushViewController(mapVC, animated: false)
        }
    }
    
    @IBAction func loginButton(_ sender: UIButton)
    {
        if txtUserField.text == "admin" && txtPassField.text == "admin"
        {
            UserDefaults.standard.set(true, forKey: "ISUSERLOGGEDIN")
            let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapVC") as! MapViewController
            self.navigationController?.pushViewController(mapVC, animated: true)
        }
        
        else
        {
            labelAlert.isHidden = false
            labelAlert.text = "Username atau Password SALAH!!"
        }
    }
}

